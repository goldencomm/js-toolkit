# GoldenComm's JavaScript Toolkit Changelog

## Current Version: 1.0.4

### 1.0.4

Released on 10 May 2021

 - Bug Fix: Fixing issue with Lightbox content being added back to DOM in the wrong location
 - Other: Fixing issue with CloudFlare's RocketLoader product interfering with DOMContentLoaded event, which causes the Toolkit to never actually run

### 1.0.3

Released on 10 Feb 2021
 - Bug fix: Updated browser support logic in Smooth Scroll

### 1.0.1
 - Enhancement: Added support for `loading="lazy"` attribute in LazyMedia module
 - Bug fix: Fixed temp classes logic in Lightbox
 - Other: Added Changelog file
 - Other: Removed the `.lightbox-ready` nested classes in Lightbox stylesheet so that custom lightbox classes can override the default `.lightbox-container` styles without the need for `!important`
 - Other: Updating test pages