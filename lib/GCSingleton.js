"use strict";
import GCBase from './GCBase';

class GCSingleton extends GCBase {
  static instance;

  constructor(selector, name) {
    selector = selector || '';
    super(selector, name);
    this.setInstance();
    return this.getInstance();
  }

  setInstance() {
    throw new Error(`setInstance function must be implemented in extended class: ${this.constructor.name}`);
  }

  getInstance() {
    throw new Error(`getInstance function must be implemented in extended class: ${this.constructor.name}`);
  }

  getExport() {
    return Object.freeze(this.getInstance());
  }
}

export {GCSingleton};
export default GCSingleton;